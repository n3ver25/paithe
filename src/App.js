import React, { Component } from 'react';
import { View, Text } from "react-native";
import { Header } from './components/common';
import Card from './components/BlogList';
import Footer from './components/Footer';
import firebase from 'firebase';
import Router from './Router';

class App extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    firebase.initializeApp({
      
        apiKey: 'AIzaSyDznuWGSIeorpX0BxKsWGa5Pjhhrq-GSNQ',
        authDomain: 'travel-5ce4f.firebaseapp.com',
        databaseURL: 'https://travel-5ce4f.firebaseio.com',
        projectId: 'travel-5ce4f',
        storageBucket: 'travel-5ce4f.appspot.com',
        messagingSenderId: '553801113805'

        
    })
  }

  render() {
    return (
      <Router />
    );
  }
}

export default App;