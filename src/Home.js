import React from 'react';
import { View, Text,ImageBackground,Image,TouchableOpacity } from "react-native";
import { Actions } from 'react-native-router-flux';



const Home = (props) => {
  return (
    <View>
    <View>
    <ImageBackground style={styles.imageStyle}
      source={require('./photo/1.jpg')}>
      <View>
        <Text style={styles.fontStyle}>Welcome to </Text><Text style={styles.fontStyle}>Chiang rai</Text>
      </View>
      <View>
    <Image style={{ width: 380,height: 380}} 
      source={require('./photo/2.png')}></Image>
      </View>
      <TouchableOpacity style={styles.rectangle} onPress={()=>Actions.Home()}>
      <Text style={{fontSize:20, color: "#ffffff"}}> Let'go</Text>
      </TouchableOpacity>
      </ImageBackground>
  </View>
  </View>
  );
}
const styles = {
  headerStyle: {
    height: 75,
    backgroundColor: "#0082AA",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#0082AA",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: "relative"
  },
  textStyle: {
    fontSize: 10,
    color: "#fff",
    fontWeight: 'bold'
  },
  imageStyle:{
    width: null,
    height: 667
  },
  fontStyle:{
    padding: 10, 
    fontSize: 42,
    textAlign: "center",
    color: "#ffffff"
  },
  image:{
    width: 296.19,
    height: 296.19

  },
  rectangle: {
    alignItems:"center",
    justifyContent:"center",
    width: 350,
    height: 60,
    backgroundColor: '#00A9DE',
    marginLeft: 30,
    borderRadius: 20
}
}
export default Home;