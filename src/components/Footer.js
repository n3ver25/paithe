import React from 'react';
import { View,Text } from "react-native";

const Footer = () => {
  return (
    <View style={styles.footerStyle}>
      <Text style={styles.textStyle}>Footer</Text>
    </View>
  );
};

const styles = {
    footerStyle: {
    height: 75,
    backgroundColor: "#0082AA",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#0082AA",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: "relative"
  },
  textStyle: {
    fontSize: 10,
    color: "#fff",
    fontWeight: 'bold'
  }
};

export default Footer;