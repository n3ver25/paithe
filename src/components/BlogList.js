import React, {Component} from 'react';
import { FlatList, StyleSheet, ScrollView, View, Text, Modal, TouchableOpacity  } from "react-native";
import { Card, CardSection, Input, Button, Spinner } from './common';
import firebase from 'firebase';
import ListItem from './ListItem';

class Bloglist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            blog: [],
            modalVisible: false,
        }
    }

    componentDidMount() {
        this.fetchBlog();
    }

    async fetchBlog() {
        let data = [];
        await firebase.database().ref('travel').on('value',
            snapshoot => {
                snapshoot.forEach(
                    snap => {
                        const value = snap.val();
                        value.id = snap.key;
                        data.push(value);
                    })
                data.reverse();
                this.setState({ blog: data });
            });

    }

    renderRow(blog) {
        return <ListItem blog={blog} />;
    }

    renderButton() {
        if (false) {
            return <Spinner size="large" />;
        }
        return (
            <Button onPress={() => this.onSubmitPress()}>
                Submit
      </Button>
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                
                <View style={styles.button}>
                    <TouchableOpacity onPress={() => { this.setState({ modalVisible: true }) }}>
                        <View style={styles.iconContainer}>
                            <Text style={{ fontSize: 30, color: "#fff" }}>+</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.setState({ modalVisible: failse })}
                >
                    <View>
                        <Card>
                            <CardSection>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 24, fontWeight: 'bold' }}>Create Blog</Text>
                                </View>
                            </CardSection>

                            <CardSection>
                                <Input
                                    label="Name"
                                    placeholder="This is name."
                                />
                            </CardSection>

                            <CardSection>
                                <Input
                                    label="Description"
                                    placeholder="Description..."
                                />
                            </CardSection>

                            <CardSection>
                                <Input
                                    label="Image"
                                    placeholder="https://yourpicture.com/yourpicture"
                                />
                            </CardSection>

                            <Text style={styles.errorTextStyle}>
                            </Text>

                            <CardSection>
                                {this.renderButton()}
                            </CardSection>

                            <CardSection>
                                <Button danger onPress={() => { this.setState({ modalVisible: false }); }}>
                                    Cancel
                </Button>
                            </CardSection>
                        </Card>
                    </View>
                </Modal>
                <ScrollView>
                    <FlatList
                        data={this.state.blog}
                        renderItem={(item) => this.renderRow(item)}
                        keyExtractor={item => item.id.toString()}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        position: "absolute",
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: "#8BBBDE",
        bottom: 25,
        zIndex: 2,
        right: 15,
        alignItems: "center",
        justifyContent: "center"
    },
    iconContainer: {
        width: 50,
        height: 50,
        alignItems: "center",
        justifyContent: "center"
    }
});

export default Bloglist;
