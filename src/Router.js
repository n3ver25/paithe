import React from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import BlogList from './components/BlogList';
import Home from './Home';

const Root = () => {


    return (
        <Router>
            <Scene key="Login" initial   >
                <Scene key="blogList"  component={Home} hideNavBar={true} />
                <Scene key="Home" component={BlogList}/>
            </Scene>
        </Router>
    )
}

export default Root;